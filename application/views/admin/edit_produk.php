<div class="container-fluid">
    <h3><i class="fas fa-edit"></i>EDIT DATA PRODUK</h3>

    <?php foreach($produk as $produk) : ?>

        <form method="post" action="<?php echo base_url().'admin/data_produk/update' ?>">
        
            <div class="for-group">
                <label>Nama Produk</label>
                <input type="text" name="nama_produk" class="form-control mb-3"
                value="<?php echo $produk->nama_produk ?>">
            </div>

            <div class="for-group">
                <label>Keterangan</label>
                <input type="hidden" name="id_produk" class="form-control"
                value="<?php echo $produk->id_produk?>">
                <input type="text" name="keterangan_produk" class="form-control mb-3"
                value="<?php echo $produk->keterangan_produk ?>">
            </div>

            <div class="for-group">
                <label>Harga</label>
                <input type="text" name="produk" class="form-control mb-3"
                value="<?php echo $produk->harga_produk ?>">
            </div>

            <div class="for-group">
                <label>Stok</label>
                <input type="text" name="stok" class="form-control mb-3"
                value="<?php echo $produk->stok ?>">
            </div>

            <button type="submit" class="btn btn-primary btn-sm mt-3">Simpan</button>
        </form>

    <?php endforeach; ?>
</div>