<?php

class Dashboard extends CI_Controller {
    public function index () 
    {
        $data['produk'] = $this->model_produk->tampil_data()->result();
        $this-> load -> view('templates/header');
        $this-> load -> view('templates/sidebar');
        $this-> load -> view('dashboard', $data);
        $this-> load -> view('templates/footer');
    }
    public function produk () 
    {
        $data['produk'] = $this->model_produk->tampil_data()->result();
        $this-> load -> view('templates/header');
        $this-> load -> view('templates/sidebar');
        $this-> load -> view('produk', $data);
        $this-> load -> view('templates/footer');
    }

    public function tambah_ke_keranjang ($id) 
    {
        $produk = $this->model_produk->find($id);

        $data = array(
            'id'      => $produk->id_produk,
            'qty'     => 1,
            'price'   => $produk->harga_produk,
            'name'    => $produk->nama_produk,
    );
    
    $this->cart->insert($data);
    redirect('dashboard');

    }
    public function detail_keranjang () 
    {
        
        $this-> load -> view('templates/header');
        $this-> load -> view('templates/sidebar');
        $this-> load -> view('keranjang');
        $this-> load -> view('templates/footer');

    }

}