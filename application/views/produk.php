<div class="container-fluid">

    <div class="row text-center mt-3">

    <?php foreach ($produk as $produk) : ?>
        
        <div class="card ml-3 mb-3" style="width: 16rem;">
        <img src="<?php echo base_url().'/uploads/'.$produk->gambar_produk ?> " class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title mb-1"><?php echo $produk-> nama_produk?> </h5>
            <span class="badge bg-info text-dark mb-3">Rp. <?php echo number_format($produk-> harga_produk), '.',0 ?></span>
            <?php echo anchor('dashboard/tambah_ke_keranjang/' .$produk ->id_produk, '<div class="btn btn-sm btn-primary mb-3"> Tambah ke Keranjang </div>')?>
            <a href="#" class="btn btn-success">Detail</a>
        </div>
    </div>
    <?php endforeach; ?>
    </div>
</div>