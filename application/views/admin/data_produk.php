<div class="container-fluid">
    <button class="btn btn-sm btn-primary mb-3" data-toggle="modal" data-target="#tambah_produk"> <i class="fas fa-plus fa-sm"></i> Tambah Produk </button>

    <table class="table table-bordered">
        <tr>
            <th>NO</th>
            <th>NAMA PRODUK</th>
            <th>KETERANGAN</th>
            <th>HARGA</th>
            <th>Stok</th>
            <th>GAMBAR</th>
            <th colspan="3">AKSI</th>
        </tr>

        <?php 
        $no=1;
        foreach ($produk as $produk) : ?>
        
        <tr>
            <td><?php echo $no++ ?></td>
            <td><?php echo $produk->nama_produk ?></td>
            <td><?php echo $produk->keterangan_produk ?></td>
            <td><?php echo $produk->harga_produk ?></td>
            <td><?php echo $produk->stok ?></td>
            <td><?php echo $produk->gambar_produk ?></td>
            <td><?php echo anchor('admin/data_produk/cari/' .$produk->id_produk, '<div class="btn btn-success btn-sm"><i class="fas fa-search-plus"><i></div>') ?></td>
            <td><?php echo anchor('admin/data_produk/edit/' .$produk->id_produk, '<div class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></div>') ?></td>
            <td><?php echo anchor('admin/data_produk/delete/' .$produk->id_produk, '<div class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></div>') ?></td>

        </tr>
        <?php endforeach; ?>

    </table>
</div>

<!-- Modal -->
<div class="modal fade" id="tambah_produk" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Form Input Data Produk</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?php echo base_url().'admin/data_produk/tambah_aksi'; ?>" method="post" enctype="multipart/form-data">

        <div class="form-group">
            <label>Nama Produk</label>
            <input type="text" name="nama_produk" class="form-control">
        </div>
        <div class="form-group">
            <label>Keterangan </label>
            <input type="text" name="keterangan_produk" class="form-control">
        </div>
        <div class="form-group">
            <label>Harga</label>
            <input type="text" name="harga_produk" class="form-control">
        </div>
        <div class="form-group">
            <label>Stok</label>
            <input type="text" name="stok" class="form-control">
        </div>
        <div class="form-group">
            <label>Gambar Produk</label><br>
            <input type="file" name="gambar_produk" class="form-control">
        </div>

        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>

      </form>

    </div>
  </div>
</div>