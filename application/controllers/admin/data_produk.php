<?php

class Data_produk extends CI_Controller{
    public function index()
    {
        $data['produk'] = $this->model_produk->tampil_data()->result();
        $this->load->view('templates_admin/header');
        $this->load->view('templates_admin/sidebar');
        $this->load->view('admin/data_produk', $data);
        $this->load->view('templates_admin/footer');
    }
    public function tambah_aksi()
    {
        $nama_produk          =$this->input->post('nama_produk');
        $keterangan_produk    =$this->input->post('keterangan_produk');
        $harga_produk         =$this->input->post('harga_produk');
        $stok                 =$this->input->post('stok');
        $gambar_produk        =$_FILES['gambar_produk']['name'];
        if ($gambar_produk = '') {} else {
            $config ['upload_path'] = './uploads';
            $config ['allowed_types'] = 'jpg|jpeg|png|gif';

            $this->load->library('upload', $config);
            if(!$this->upload->do_upload('gambar_produk')){
                echo "Gambar Gagal di Upload!";
            } else {
                $gambar_produk=$this->upload->data('file_name');
            }
        }
        $data = array (
            'nama_produk'          => $nama_produk,
            'keterangan_produk'    => $keterangan_produk,
            'harga_produk'         => $harga_produk,
            'stok'                 => $stok,
            'gambar_produk'        => $gambar_produk
        );

        $this->model_produk->tambah_produk($data, 'produk');
        redirect('admin/data_produk/index');

    }
    public function edit ($id)
    {
        $where = array ('id_produk' => $id);
        $data['produk'] = $this->model_produk->edit_produk($where, 'produk')->result();
        
        $this->load->view('templates_admin/header');
        $this->load->view('templates_admin/sidebar');
        $this->load->view('admin/edit_produk', $data);
        $this->load->view('templates_admin/footer');
    }

    public function update(){
        $id                 =$this->input->post('id_produk');
        $nama_produk        =$this->input->post('nama_produk');
        $keterangan_produk  =$this->input->post('keterangan_produk');
        $harga_produk       =$this->input->post('harga_produk');
        $stok               =$this->input->post('stok');

        $data = array (
            'nama_produk'          => $nama_produk,
            'keterangan_produk'    => $keterangan_produk,
            'harga_produk'         => $harga_produk,
            'stok'                 => $stok,
            'gambar_produk'        => $gambar_produk
        );

        $where = array(
            'id_produk' => $id
        );
        $this->model_produk->update_data($where,$data,'produk');
        redirect('admin/data_produk/index');
    }
    public function delete ($id)
    {
        $where = array ('id_produk' => $id);
        $this->model_produk->delete_data($where, 'produk');
        redirect('admin/data_produk/index');
    }
}